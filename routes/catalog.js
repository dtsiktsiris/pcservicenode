var express = require('express');
var router = express.Router();

var customer_controller = require('../controllers/customerController');
var service_controller = require('../controllers/serviceController');
var main_controller = require('../controllers/mainController');
var serviceprocess_controller = require('../controllers/serviceProcessController');
var company_controller = require('../controllers/companyController');

/// CUSTOMER ROUTES ///

// GET request for creating Author. NOTE This must come before route for id (i.e. display author).
router.get('/customer/create', customer_controller.customer_create_get);

// POST request for creating Author.
router.post('/customer/create', customer_controller.customer_create_post);

// GET request to delete Author.
router.get('/customer/:id/delete', customer_controller.customer_delete_get);

// POST request to delete Author.
router.post('/customer/:id/delete', customer_controller.customer_delete_post);

// GET request to update Author.
router.get('/customer/:id/update', customer_controller.customer_update_get);

// POST request to update Author.
router.post('/customer/:id/update', customer_controller.customer_update_post);

// GET request for one Author.
router.get('/customer/:id', customer_controller.customer_detail);

// GET request for list of all Authors.
router.get('/customers', customer_controller.customer_list);

// GET request for list of all Authors.
router.get('/dashboard', main_controller.dashboard);

router.get('/login', main_controller.login);

router.get('/logout', main_controller.logout);

router.post('/check_login', main_controller.check_login);

// GET request for list of all Authors.
router.get('/services', service_controller.service_list);

router.get('/service/create', service_controller.service_create_get);

router.post('/service/create', service_controller.service_create_post);

router.get('/service/:id', service_controller.service_detail);

router.post('/serviceprocess/create', serviceprocess_controller.serviceprocess_create_post);

router.get('/company/create', company_controller.company_create_get);

router.post('/company/create', company_controller.company_create_post);

module.exports = router;