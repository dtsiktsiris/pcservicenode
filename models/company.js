var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CompanySchema = new Schema(
    {
        first_name: {type: String, required: true, max: 100},
        last_name: {type: String, required: true, max: 100},
        tel_home: {type: String, max: 25},
        tel_mobile: {type: String, required: true, max: 25},
        address: {type: String, required: true, max: 100},
        city: {type: String, required: true, max: 50},
        postal_code: {type: String, required: true, max: 10},
        email: {type: String, required: true, max: 100},
        password: {type: String, required: true},
        brand_name: {type: String, max: 100},
        tax_id: {type: String, max: 100},
        profession: {type: String, max: 100},
        subscription_date: {type: String, min: 10, max: 10},
    }
);

// Virtual for author's full name
CompanySchema
    .virtual('name')
    .get(function () {
        return this.last_name + ' ' + this.first_name;
    });

// Virtual for author's URL
CompanySchema
    .virtual('url')
    .get(function () {
        return '/app/customer/' + this._id;
    });

//Export model
module.exports = mongoose.model('Company', CompanySchema);