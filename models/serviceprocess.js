var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ServiceProcessSchema = new Schema(
  {
    process_info: {type: String, required: true, max: 200},
    service: {type: Schema.Types.ObjectId, ref: 'Service', required: true},
    date: {type: String, min: 10, max: 10, required: true},
  }
);

// Virtual for author's URL
ServiceProcessSchema
.virtual('url')
.get(function () {
  return '/catalog/author/' + this._id;
});

//Export model
module.exports = mongoose.model('ServiceProcess', ServiceProcessSchema);