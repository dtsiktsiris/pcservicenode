var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var OrderSchema = new Schema(
  {
    shop: {type: String, required: true, max: 100},
    link: {type: String, required: true, max: 100},
    status: {type: String, max: 25},
    description: {type: String, required: true, max: 200},
    customer: {type: Schema.Types.ObjectId, ref: 'Customer'},
    service: {type: Schema.Types.ObjectId, ref: 'Service'},
    date_order: {type: String, min: 10, max: 10},
  }
);

// Virtual for author's URL
OrderSchema
.virtual('url')
.get(function () {
  return '/catalog/author/' + this._id;
});

//Export model
module.exports = mongoose.model('Order', OrderSchema);