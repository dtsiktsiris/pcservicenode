var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ServiceSchema = new Schema(
  {
    incremental_number: {type: Number, unique: true, required: true},
    type: {type: String, required: true, max: 100},
    accessories: {type: String, required: true},
    status: {type: String, required: true, enum: ['Received', 'In Process', 'Done', 'To Be Delevered', 'Delivered'], default: 'Received'},
    cost: {type: String, required: true, max: 5},
    description: {type: String, required: true, max: 500},
    spare_part: {type: Boolean, default: false},
    date_received: {type: String, min: 10, max: 10, required: true},
    date_delivered: {type: String, min: 10, max: 10},
    customer: {type: Schema.Types.ObjectId, ref: 'Customer', required: true},
  }
);

// Virtual for author's URL
ServiceSchema
.virtual('url')
.get(function () {
  return '/app/service/' + this._id;
});

//Export model
module.exports = mongoose.model('Service', ServiceSchema);