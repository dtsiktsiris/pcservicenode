var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CustomerSchema = new Schema(
  {
    first_name: {type: String, required: true, max: 100},
    last_name: {type: String, required: true, max: 100},
    tel_home: {type: String, max: 25},
    tel_mobile: {type: String, required: true, max: 25},
    address: {type: String, required: true, max: 100},
    city: {type: String, required: true, max: 50},
    postal_code: {type: String, required: true, max: 10},
    email: {type: String, required: true, max: 100},
    brand_name: {type: String, max: 100},
    tax_id: {type: String, max: 100},
    profession: {type: String, max: 100},
    date_of_birth: {type: String, min: 10, max: 10},
    company: {type: Schema.Types.ObjectId, ref: 'Company', required: true},
  }
);

// Virtual for author's full name
CustomerSchema
.virtual('name')
.get(function () {
  return this.last_name + ' ' + this.first_name;
});

// Virtual for author's URL
CustomerSchema
.virtual('url')
.get(function () {
  return '/app/customer/' + this._id;
});

//Export model
module.exports = mongoose.model('Customer', CustomerSchema);