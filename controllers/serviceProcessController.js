const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');
var ServiceProcess = require('../models/serviceprocess');

exports.serviceprocess_create_post = [

    body('service_id').isLength({ min: 2 }).trim().withMessage('First name must be specified.'),

    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()){
            res.render('service_list', { title: 'Create Service', service: req.body, errors: errors.array() });
            return;
        }
        else {
            var date = req.body.date.split('-');
            date = date[2]+'/'+date[1]+'/'+date[0];

            var serviceProcess = new ServiceProcess(
                {
                    service: req.body.service_id,
                    date: date,
                    process_info: req.body.process_info,
                });
            serviceProcess.save(function (err) {
                if (err) { return next(err); }
                // Successful - redirect to new author record.
                res.redirect('service_list');
            });
        }

    }
];