const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');
var Customer = require('../models/customer');
var Company = require('../models/company');

var DTUtils = require('../dtUtils/utils');

exports.dashboard = function(req, res){

    if (!req.session.email){
        res.redirect('/app/login');
    }
    else {
        Customer.countDocuments({},)
            .exec(function (err, customers_count) {
                if (err) {
                    return next(err);
                }
                res.locals.email = req.session.email
                res.render('dashboard', {title: 'Dashboard', customers_count: customers_count});
            });
    }
};

exports.login = function(req, res){
    if (!req.session.email){
        res.render('login', {title : 'Login'});
    }
    else {
        res.redirect('/app/dashboard');
    }
};

exports.check_login = function(req, res, next){

    async.parallel({
        company: function(callback) {
            Company.find({ email: req.body.email, password: req.body.password})
                .exec(callback)
        }

    }, function(err, results) {
        if (err) { return next(err); } // Error in API usage.
        if (results.company==null) { // No results.
            var err = new Error('Company not found');
            err.status = 404;
            return next(err);
        }
        req.session.email = results.company[0].email;
        req.session._id = results.company[0]._id;
        console.log(results.company[0]._id);
        console.log(req.session._id);
        res.redirect('/app/dashboard');
    });
}

exports.logout = function(req, res){

    req.session.email = null;
    req.session._id = null;
    res.redirect('/app/dashboard');
}