const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');
var Customer = require('../models/customer');
var Service = require('../models/service');
var ServiceProcess = require('../models/serviceprocess');

var DTUtils = require('../dtUtils/utils');

// Display list of all Authors.
exports.service_list = function(req, res) {

    console.debug(req.query.search);

    Service.find()
        .exec(function ( err, list_services) {
            if (err) { return next(err); }
            res.render('service_list', {title : 'Service List', service_list: list_services});
        });
};

exports.service_create_get = function(req, res, next) {

    today = DTUtils.dateForHtml();

    async.parallel({
        customers: function(callback) {
            Customer.find(callback);
        },
        service: function(callback){
            Service.find()
                .select('incremental_number')
                .sort({'incremental_number': -1})
                .limit(1)
                .exec(callback);
        },
    }, function(err, results) {
            if (err) {return next(err);}
            var next = results.service[0].incremental_number+1;
            res.render('service_form', { title: 'Create Service',customers: results.customers, today: today, next: next});
    });
};

// Handle service create on POST.
exports.service_create_post = [
    
    body('type').isLength({ min: 2 }).trim().withMessage('First name must be specified.')
        .isAlphanumeric().withMessage('Type has non-alphanumeric characters.'),

    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()){
            res.render('service_form', { title: 'Create Service', service: req.body, errors: errors.array() });
            return;
        }
        else {

            date = DTUtils.dateUsToGr(req.body.date_received);

            var service = new Service(
                {
                    incremental_number: req.body.incremental_number,
                    customer: req.body.customer,
                    type: req.body.type,
                    accessories: req.body.accessories,
                    status: req.body.status,
                    cost: req.body.cost,
                    description: req.body.description,
                    spare_part: req.body.spare_part,
                    date_received: date,
                    date_delivered: req.body.date_delivered,
                });
                service.save(function (err) {
                if (err) { return next(err); }
                // Successful - redirect to new author record.
                res.redirect(service.url);
            });
        }
        
    }
];

exports.service_detail = function(req, res, next) {
    var today = new Date();
    var d = today.getDate()+1;
    var m = today.getMonth()+1;
    if (m <= 9){
        m= '0'+m;
    }
    if (d <= 9){
        d= '0'+d;
    }
    today = today.getFullYear()+'-'+m+'-'+d;

    async.parallel({
        service: function(callback) {
            Service.findById(req.params.id)
                .exec(callback)
        },
        serviceprocesses: function(callback) {
            ServiceProcess.find({ service: req.params.id})
                .exec(callback)
        }

    }, function(err, results) {
        if (err) { return next(err); } // Error in API usage.
        if (results.service==null) { // No results.
            var err = new Error('Service not found');
            err.status = 404;
            return next(err);
        }
        // Successful, so render.
        res.render('service_detail', { title: 'service Detail', service: results.service, serviceprocesses: results.serviceprocesses, today: today } );
    });
};