const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');
var Company = require('../models/company');

exports.company_create_get = function(req, res, next) {
    res.render('company_form', { title: 'Create Company'});
};

// Handle Author create on POST.
exports.company_create_post = [

    body('first_name').isLength({ min: 1 }).trim().withMessage('First name must be specified.')
        .isAlphanumeric().withMessage('First name has non-alphanumeric characters.'),
    body('last_name').isLength({ min: 1 }).trim().withMessage('Family name must be specified.')
        .isAlphanumeric().withMessage('Family name has non-alphanumeric characters.'),

    sanitizeBody('first_name').trim(),
    sanitizeBody('last_name').trim(),

    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()){
            res.render('company_form', { title: 'Create Company', company: req.body, errors: errors.array() });
            return;
        }
        else {
            var date = req.body.subscription_date.split('-');
            date = date[2]+'/'+date[1]+'/'+date[0];
            var company = new Company(
                {
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    tel_home: req.body.tel_home,
                    tel_mobile: req.body.tel_mobile,
                    address: req.body.address,
                    city: req.body.city,
                    postal_code: req.body.postal_code,
                    email: req.body.email,
                    password: req.body.password,
                    brand_name: req.body.brand_name,
                    tax_id: req.body.tax_id,
                    profession: req.body.profession,
                    subscription_date: date,
                });
            company.save(function (err) {
                if (err) { return next(err); }
                // Successful - redirect to new author record.
                res.redirect(company.url);
            });
        }

    }
];