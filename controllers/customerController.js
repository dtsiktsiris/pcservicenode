const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');
var Customer = require('../models/customer');
var Service = require('../models/service');

// Display list of all Authors.
exports.customer_list = function(req, res) {

    console.debug(req.query.search);

    Customer.find({last_name: { $regex: '.*' + req.query.search + '.*', $options: 'i' } }, )
        .exec(function ( err, list_customers) {
            if (err) { return next(err); }
            res.render('customer_list', {title : 'Customers', customer_list: list_customers});
        });
};

exports.customer_detail = function(req, res, next) {
    async.parallel({
        customer: function(callback) {
            Customer.findById(req.params.id)
                .exec(callback)
        },
        services: function(callback) {
            Service.find({ customer: req.params.id})
                .exec(callback);
        },

    }, function(err, results) {
        if (err) { return next(err); } // Error in API usage.
        if (results.customer==null) { // No results.
            var err = new Error('Customer not found');
            err.status = 404;
            return next(err);
        }
        
        //console.log(dat);
        // Successful, so render.
        res.render('customer_detail', { title: 'customer Detail', customer: results.customer, services: results.services } );
    });
};

// Display Author create form on GET.
exports.customer_create_get = function(req, res, next) {
    res.render('customer_form', { title: 'Create Customer'});
};

// Handle Author create on POST.
exports.customer_create_post = [
    
    body('first_name').isLength({ min: 1 }).trim().withMessage('First name must be specified.')
        .isAlphanumeric().withMessage('First name has non-alphanumeric characters.'),
    body('last_name').isLength({ min: 1 }).trim().withMessage('Family name must be specified.')
        .isAlphanumeric().withMessage('Family name has non-alphanumeric characters.'),

    sanitizeBody('first_name').trim(),
    sanitizeBody('last_name').trim(),
    
    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()){
            res.render('customer_form', { title: 'Create Customer', customer: req.body, errors: errors.array() });
            return;
        }
        else {
            var date = req.body.date_of_birth.split('-');
            date = date[2]+'/'+date[1]+'/'+date[0];

            console.log(req.session._id);

            var customer = new Customer(
                {
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    tel_home: req.body.tel_home,
                    tel_mobile: req.body.tel_mobile,
                    address: req.body.address,
                    city: req.body.city,
                    postal_code: req.body.postal_code,
                    email: req.body.email,
                    brand_name: req.body.brand_name,
                    tax_id: req.body.tax_id,
                    profession: req.body.profession,
                    date_of_birth: date,
                    company: req.session._id,
                });
            customer.save(function (err) {
                if (err) { return next(err); }
                // Successful - redirect to new author record.
                res.redirect(customer.url);
            });
        }
        
    }
];

// Display Author delete form on GET.
exports.customer_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Customer delete GET');
};

// Handle Author delete on POST.
exports.customer_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Customer delete POST');
};

// Display Author update form on GET.
exports.customer_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Customer update GET');
};

// Handle Author update on POST.
exports.customer_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Customer update POST');
};