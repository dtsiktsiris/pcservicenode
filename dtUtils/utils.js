exports.dateUsToGr = function( usDate){
    var date = usDate.split('-');
    return date[2]+'/'+date[1]+'/'+date[0];
}

exports.dateForHtml = function(){
    var today = new Date();
    var d = today.getDate()+1;
    var m = today.getMonth()+1;
    if (m <= 9){
        m= '0'+m;
    }
    if (d <= 9){
        d= '0'+d;
    }
    return today.getFullYear()+'-'+m+'-'+d;
}
